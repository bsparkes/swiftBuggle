//
//  GameWord.swift
//  buggle
//
//  Created by Benjamin Sparkes on 23/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import Foundation

class GameWord : Hashable, Codable {
  
  let word: String
  let points: Int
  var found: Bool
  
  init(word w: String) {
    word = w
    points = calculatePoints(word: w)
    found = false
  }
  
  init(word w: String, found f: Bool) {
    word = w
    points = calculatePoints(word: w)
    found = f
  }
  
  static func == (lhs: GameWord, rhs : GameWord) -> Bool {
    return lhs.word == rhs.word
  }
  
  func hash(into hasher: inout Hasher) {
    hasher.combine(word)
    hasher.combine(points)
    hasher.combine(found)
  }
  
}

func calculatePoints(word: String) -> Int {
  
  guard word.count != 0 else {
    return 0
  }
  
  switch word.count {
  case 3:
    return 1
  case 4:
    return 1
  case 5:
    return 2
  case 6:
    return 3
  case 7:
    return 5
  default:
    return 11
  }
  
}
