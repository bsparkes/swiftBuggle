//
//  TileIndex.swift
//  buggle
//
//  Created by Benjamin Sparkes on 23/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import Foundation

struct TileIndex : Hashable, Codable {
  
  private enum CodingKeys: String, CodingKey {
    case row, col
  }
  
  var row : Int
  var col : Int
  
  static func == (lhs : TileIndex, rhs : TileIndex) -> Bool {
    return lhs.row == rhs.row && lhs.col == rhs.col
  }
  
  func hash(into hasher: inout Hasher) {
    hasher.combine(col)
    hasher.combine(row)
  }
  
}
