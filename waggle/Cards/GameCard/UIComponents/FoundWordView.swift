//
//  FoundWordView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 25/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class FoundWordView: UITableView {
  
  var wordList: [GameWord]
  
  private var listWidth: CGFloat
  private var listHeight: CGFloat
  private var cornerRadius: CGFloat
  private let shadeWords: Bool
  private var foundWordObserver: Any!
  
  let fontSize: CGFloat
  
  deinit {
    print("FoundWordView deleted")
  }
  
  init(listWidth width: CGFloat, listHeight height: CGFloat, cornerRadius radius: CGFloat, shadeWords shade: Bool) {
    listWidth = width
    listHeight = height
    cornerRadius = radius
    shadeWords = shade
    wordList = [GameWord]()
    fontSize = width*0.09
    
    super.init(frame: CGRect(x: 0, y: 0, width: listWidth, height: listHeight), style: .plain)
    
    sharedInit(radius: radius)
  }
  
  
  init(listWidth width: CGFloat, listHeight height: CGFloat, cornerRadius radius: CGFloat, shadeWords shade: Bool, wordList wl: [GameWord]) {
    listWidth = width
    listHeight = height
    cornerRadius = radius
    shadeWords = shade
    wordList = wl
    fontSize = width*0.09
    
    super.init(frame: CGRect(x: 0, y: 0, width: listWidth, height: listHeight), style: .plain)
    
    sharedInit(radius: radius)
  }
  
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  func sharedInit(radius r: CGFloat) {
    layer.cornerRadius = r
    
    backgroundColor = gameCardElementBackgroundColour
    separatorStyle = .none
    dataSource = self
    delegate = self
    rowHeight =  UIFont(name: textFontName, size: fontSize)!.lineHeight
    
    allowsSelection = false
    indicatorStyle = UIScrollView.IndicatorStyle.black
  }
  
}

extension FoundWordView: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return wordList.count
  }
  
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    // Getting the right element
    let word = wordList[indexPath.row]
    
    // Trying to reuse a cell
    let cellIdentifier = word.word
    let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier)
      ?? UITableViewWordCell(style: .default, reuseIdentifier: cellIdentifier, word: word, width: listWidth, height: rowHeight, shadeWords: shadeWords)
    return cell
  }
  
  
  // This fades a row if it's been selected. Should figure out how to disable this.
//  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//    tableView.deselectRow(at: indexPath, animated: true)
//  }
  
  // Later, I should add some kind of gradient, and this is one way to do it.
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    cell.backgroundColor = UIColor.clear
  }
  
  
  public func updateAndScroll(word: GameWord) {
    var wordIndex: Int
    
    if wordList.contains(word) {
      wordIndex = wordList.firstIndex(of: word)!
    } else {
      wordIndex = wordList.count
      wordList.append(word)
      reloadData()
    }
    scrollToRow(at: [0, wordIndex], at: .bottom, animated: true)
  }
  
  
  public func listUpdateAndScroll(updateList: [GameWord]) {
    for word in updateList { wordList.append(word) }
    reloadData()
    if (wordList.count > 0) { scrollToRow(at: [0, wordList.count - 1], at: .bottom, animated: false) } // Count is total, etc.
  }
  
  
  public func clearWords() {
    wordList.removeAll()
    reloadData()
  }
  
  
  private func updateFromNotification(Notification: Notification) -> Void {
    guard let word = Notification.userInfo?["word"] as? GameWord else { return }
    updateAndScroll(word: word)
  }
  
  
  public func enableModelObserver() {
    foundWordObserver = NotificationCenter.default.addObserver(forName: .didFindWord, object: nil, queue: OperationQueue.main, using: updateFromNotification)
  }
  
  
  public func disableModelObserver() {
    if foundWordObserver != nil {
      NotificationCenter.default.removeObserver(foundWordObserver!)
    }
  }
  
}
