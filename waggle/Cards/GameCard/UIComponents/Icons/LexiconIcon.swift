//
//  LexiconIcon.swift
//  buggle
//
//  Created by Benjamin Sparkes on 18/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit


class LexiconIcon: IconView {
  
  let bookHeight: Double
  let bookWidth: Double
  let bookSquish = Double(1.5)
  let bookEllipseAngle = Double.pi/8
  
  let lambdaStrokeWidth: CGFloat
  let bookStrokeWidth: CGFloat
  
  let bookColour = UIColor.lightGray.cgColor
  let lambdaColour = UIColor.darkGray.cgColor
  
  let bookBaseX: Double
  let bookBaseY: Double
  let bookBasePoint: CGPoint
  
  var censorLayer: CAShapeLayer?
  
  
  override init(size s: CGFloat) {
    
    bookHeight = Double(s*0.7)
    bookWidth = Double((bookHeight * 2)/3)
    
    lambdaStrokeWidth = CGFloat(bookHeight)/12
    bookStrokeWidth = CGFloat(bookHeight)/15
    
    bookBaseX = Double(s*0.38)
    bookBaseY = Double(s*0.96)

    bookBasePoint = CGPoint(x: bookBaseX, y: bookBaseY)
    
    super.init(size: s)
    
    backgroundColor = UIColor.clear
    addBook()
    addLambda()
    updateIcon(value: global.gameConfig.getCurrentOption(option: global.gameConfig.lexiconID, new: false))
  }
  
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func updateIcon(value v: Int) {
    switch v {
    case 1:
      addCensorLayer()
    default:
      removeCensorLayer()
    }
  }
  
  
  func addBook() {
    
    let cover = UIBezierPath()
    cover.move(to: bookBasePoint)
    
    let bookBaseEndPoint = getEllipsePoint(origin: bookBasePoint, radius: bookWidth, squish: bookSquish, theta: bookEllipseAngle)
    
    cover.addLine(to: bookBaseEndPoint)
    
    let bookTopEndPoint = CGPoint(x: Double(bookBaseEndPoint.x), y: Double(bookBaseEndPoint.y) - bookHeight)
    
    cover.addLine(to: bookTopEndPoint)
    
    let bookTopPoint = getEllipsePoint(origin: bookTopEndPoint, radius: bookWidth, squish: bookSquish, theta: Double.pi + bookEllipseAngle)
    
    cover.addLine(to: bookTopPoint)
    
    cover.addLine(to: bookBasePoint)
    
    let coverLayer = CAShapeLayer()
    coverLayer.path = cover.cgPath
    coverLayer.strokeColor = bookColour
    coverLayer.lineWidth = bookStrokeWidth
    coverLayer.fillColor = bookColour
    
    
    let spine = UIBezierPath()
    spine.move(to: bookBasePoint)
    
    let spineBaseEndPoint = getEllipsePoint(origin: bookBasePoint, radius: bookWidth/3, squish: bookSquish, theta: Double.pi - bookEllipseAngle)
    spine.addLine(to: spineBaseEndPoint)
    
    let spineTopPoint = CGPoint(x: Double(spineBaseEndPoint.x), y: Double(spineBaseEndPoint.y) - bookHeight)
    
    spine.addLine(to: spineTopPoint)
    spine.addLine(to: bookTopPoint)
    
    let spineLayer = CAShapeLayer()
    spineLayer.path = spine.cgPath
    spineLayer.strokeColor = bookColour
    spineLayer.lineWidth = bookStrokeWidth
    spineLayer.fillColor = bookColour
    
    let back = UIBezierPath()
    back.move(to: spineTopPoint)
    
    let backEndPoint = getEllipsePoint(origin: spineTopPoint, radius: bookWidth, squish: bookSquish, theta: bookEllipseAngle)
    back.addLine(to: backEndPoint)
    
    let backLayer = CAShapeLayer()
    backLayer.path = back.cgPath
    backLayer.strokeColor = bookColour
    backLayer.lineWidth = bookStrokeWidth
    backLayer.fillColor = bookColour
    
    coverLayer.lineJoin = .round
    spineLayer.lineJoin = .round
    backLayer.lineJoin = .round
    
    layer.addSublayer(coverLayer)
    layer.addSublayer(spineLayer)
    layer.addSublayer(backLayer)
  }
  
  
  func addLambda() {
    let lambaShort = UIBezierPath()
    
    let lambdaShortBasePoint = getEllipsePoint(origin: bookBasePoint, radius: bookWidth*0.4, squish: bookSquish, theta: (Double.pi/2 + bookEllipseAngle)/2)
    
    lambaShort.move(to: lambdaShortBasePoint)
    
    let lambaMindPoint = getEllipsePoint(origin: lambdaShortBasePoint, radius: bookWidth, squish: bookSquish, theta: bookEllipseAngle + Double.pi/4)
    
    lambaShort.addLine(to: lambaMindPoint)
    
    let lambdaShortLayer = CAShapeLayer()
    lambdaShortLayer.path = lambaShort.cgPath
    lambdaShortLayer.strokeColor = lambdaColour
    lambdaShortLayer.lineWidth = lambdaStrokeWidth
    lambdaShortLayer.fillColor = lambdaColour
        
    let lambdaLong = UIBezierPath()
    
    let lambdaLongBasePoint = getEllipsePoint(origin: lambdaShortBasePoint, radius: bookWidth * 0.6, squish: bookSquish, theta: bookEllipseAngle)
    
    lambdaLong.move(to: lambdaLongBasePoint)
    
    let bookBaseEndPoint = getEllipsePoint(origin: bookBasePoint, radius: bookWidth, squish: bookSquish, theta: bookEllipseAngle)
    let bookTopEndPoint = CGPoint(x: Double(bookBaseEndPoint.x), y: Double(bookBaseEndPoint.y) - bookHeight)
    let bookTopPoint = getEllipsePoint(origin: bookTopEndPoint, radius: bookWidth, squish: bookSquish, theta: Double.pi + bookEllipseAngle)
    
    let arg = CGPoint(x: Double(bookTopPoint.x), y: Double(bookTopPoint.y))
    
    let lambdaLongTopPoint = getEllipsePoint(origin: arg, radius: bookWidth*0.3, squish: bookSquish, theta: -(Double.pi/2 - bookEllipseAngle)/2)
    
    lambdaLong.addLine(to: lambdaLongTopPoint)
        
    let lambdaLongLayer = CAShapeLayer()
    lambdaLongLayer.path = lambdaLong.cgPath
    lambdaLongLayer.strokeColor = lambdaColour
    lambdaLongLayer.lineWidth = lambdaStrokeWidth
    lambdaLongLayer.fillColor = lambdaColour
        
    lambdaShortLayer.lineCap = .round
    lambdaLongLayer.lineCap = .round
    
    lambdaShortLayer.lineJoin = .round
    lambdaLongLayer.lineJoin = .round
    
    layer.addSublayer(lambdaShortLayer)
    layer.addSublayer(lambdaLongLayer)
  }
  
  func addCensorLayer() {
    
    guard (censorLayer == nil) else {
//      logw(m: "attempting to add a second censor bar")
      return
    }
    
    let censorBarBase = CGPoint(x: bookBaseX + bookWidth * 0.1, y: bookBaseY - bookHeight * 0.4)
    
    let censorBar = UIBezierPath()
    censorBar.move(to: censorBarBase)
    let censorBaseEnd = getEllipsePoint(origin: censorBarBase, radius: bookWidth * 0.85, squish: bookSquish, theta: bookEllipseAngle)
    censorBar.addLine(to: censorBaseEnd)
    censorBar.addLine(to: CGPoint(x: censorBaseEnd.x, y: censorBaseEnd.y - CGFloat(bookHeight * 0.2)))
    censorBar.addLine(to: CGPoint(x: censorBarBase.x, y: censorBarBase.y - CGFloat(bookHeight * 0.2)))

    censorLayer = CAShapeLayer()
    censorLayer!.path = censorBar.cgPath
    censorLayer!.fillColor = lambdaColour
    censorLayer!.strokeColor = lambdaColour
    
    layer.addSublayer(censorLayer!)
  }
  
  func removeCensorLayer() {
    guard (censorLayer != nil) else {
//      logw(m: "attempting to remove non existing censor")
      return
    }
    censorLayer?.removeFromSuperlayer()
    censorLayer = nil
  }
}


func getEllipsePoint(origin: CGPoint, radius: Double, squish: Double, theta: Double) -> CGPoint {
  // See the following link for details
  // https://math.stackexchange.com/questions/22064/calculating-a-point-that-lies-on-an-ellipse-given-an-angle/22067#22067
  
  let a = radius
  let b = radius/squish
  let ab = a * b
  
  let aSinTheta = a * sin(theta)
  let bCosTheta = b * cos(theta)
  
  let denom = sqrt((bCosTheta * bCosTheta) + (aSinTheta * aSinTheta))
  
  var xPos = ((ab * cos(theta)) / denom)
  var yPos = ((ab * sin(theta)) / denom)
  
  if !(Double.pi/2 < theta || theta < (Double.pi*2)/3) {
    xPos = -xPos
    yPos = -yPos
  }
  
  xPos = xPos + Double(origin.x)
  yPos = Double(origin.y) - yPos
  
  return CGPoint(x: xPos, y: yPos)
}
