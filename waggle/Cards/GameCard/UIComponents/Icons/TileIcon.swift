//
//  TileIcon.swift
//  buggle
//
//  Created by Benjamin Sparkes on 18/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit

class TileIcon: IconView {
  
  let tileColour = UIColor.lightGray.cgColor
  let textColour = UIColor.darkGray.cgColor
  
  let textStrokeWidth: CGFloat
  
  let centerPoint: CGPoint
  let radius: CGFloat
  
  override init(size s: CGFloat) {
    
    textStrokeWidth = CGFloat(s)/12
    centerPoint = CGPoint(x: s/2, y: s/2)
    radius = CGFloat(s * 0.2)
    
    super.init(size: s)
    
    updateIcon(value: global.gameConfig.getCurrentOption(option: global.gameConfig.tilesID, new: false))
  }
  
  override func updateIcon(value v: Int) {
    
    if layer.sublayers != nil {
      for lay in layer.sublayers! {
        lay.removeFromSuperlayer()
      }
    }
    addTile()
    addBars(number: v) // - (gameConfig.getOptions(identifer: gameConfig.tilesID).first ?? 0) + 1)
  }
  
  
  func addTile() {
    let tile = UIBezierPath(roundedRect: CGRect(origin: CGPoint(x: size*0.1, y: size*0.1), size: CGSize(width: size*0.8, height: size*0.8)), cornerRadius: radius)
    
    let tileLayer = CAShapeLayer()
    tileLayer.path = tile.cgPath
    
    tileLayer.fillColor = tileColour
    tileLayer.strokeColor = tileColour
    tileLayer.lineWidth = 0
    
    layer.addSublayer(tileLayer)
  }
  
  
  func addBars(number n: Int) {
    
    let totalLines = CGFloat(n)
    let frameSize = (size * 0.75)
    let diagonal = (2 * (frameSize * frameSize)).squareRoot()
    let diagDivided = diagonal/(totalLines + 1)
    let diagXYSplit = ((diagDivided * diagDivided)/2).squareRoot()
    
    let even = n.quotientAndRemainder(dividingBy: 2).remainder == 0
    let iterations = even ? n/2 : (n - 1)/2 + 1
    let xIndent =  size*0.125 + textStrokeWidth
    
    var offset = even ? diagXYSplit : 0
    
    let line = UIBezierPath()
    
    let offsetJump = diagXYSplit * 2
    
    for _ in 1...iterations {
      line.move(to: CGPoint(x: xIndent, y: offset + xIndent))
      line.addLine(to: CGPoint(x: size - offset - xIndent, y: size - xIndent))
      // Second bar (when offset is 0 this is the same as the first)
      line.move(to: CGPoint(x: offset + xIndent, y: xIndent))
      line.addLine(to: CGPoint(x: size - xIndent, y: size - offset - xIndent))
      
      offset += offsetJump
    }
    // All lines can belong to the same layer
    let lineLayer = CAShapeLayer()
    lineLayer.path = line.cgPath
    lineLayer.strokeColor = textColour
    lineLayer.lineWidth = textStrokeWidth
    lineLayer.lineCap = .round
    
    layer.addSublayer(lineLayer)
  }
  
//  func addText(text t: String) {
//
//    for lay in textLayers {
//      lay.removeFromSuperlayer()
//    }
//    textLayers.removeAll()
//
//    let fontSizeAdjust = getFontPixelSize(fontSize: 12) * size * 0.5
//
//    let tileFont = UIFont(name: uiFontName, size: fontSizeAdjust)!
//
//    let letterLayers = getStringLayers(text: t, font: tileFont)
//
//    let yDiff = letterLayers[0].path!.boundingBox.height
//
//    var xDiff = CGFloat(0)
//    for lay in letterLayers {
//      xDiff = xDiff + lay.path!.boundingBox.width
//    }
//    var xOff = (size - xDiff)/2
//
//    for lay in letterLayers {
//
//      lay.position.x = xOff //(size - xDiff)/2 - lay.path!.boundingBox.minX
//      xOff = xOff + lay.path!.boundingBox.width
//      lay.position.y = (size - yDiff)/2 - lay.path!.boundingBox.minY
//      lay.fillColor = textColour
//      lay.strokeColor = textColour
//
//      textLayers.append(lay)
//      layer.addSublayer(lay)
//    }
//  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
}

