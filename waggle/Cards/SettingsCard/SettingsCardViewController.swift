//
//  settingsCardViewController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 03/07/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

class SettingsCardViewController: CardViewController {

  private var updateLengthObserver: Any?
  private var updateTimeObserver: Any?
  private var updateLexiconObserver: Any?
  private var updateTilesObserver: Any?
  
  override init(cardName name: String, cardWidth width: CGFloat, cardColour colour: CGColor) {
    
    super.init(cardName: name, cardWidth: width, cardColour: colour)
    
    displayStatusBar()
  }
  
  
  private func displayStatusBar() {
    // Add the relevant items to the status bar, with appropriate values.
    let adjustedLabelWidth = card.statusBar.frame.width*0.225
    let statusBarWidth = card.statusBar.frame.width
    let statusBarHeight = card.statusBar.frame.height
    
    let sep = (statusBarWidth - (4 * statusBarHeight))/5
    let xAdj = sep + statusBarHeight
    var adjSep = sep
    
    card.statusBar.addElement(elementName: global.gameConfig.lengthID, elementValue: global.gameConfig.getCurrentOptionString(option: global.gameConfig.lengthID, new: false), labelWidth: adjustedLabelWidth, xPosition: sep, icon: LengthIcon(size: statusBarHeight))
    adjSep += xAdj
    card.statusBar.addElement(elementName: global.gameConfig.timeID, elementValue: global.gameConfig.getCurrentOptionString(option: global.gameConfig.timeID, new: false), labelWidth: adjustedLabelWidth, xPosition: adjSep, icon: TimeIcon(size: statusBarHeight))
    adjSep += xAdj
    card.statusBar.addElement(elementName: global.gameConfig.lexiconID, elementValue: global.gameConfig.getCurrentOptionString(option: global.gameConfig.lexiconID, new: false), labelWidth: adjustedLabelWidth, xPosition: adjSep, icon: LexiconIcon(size: statusBarHeight))
    adjSep += xAdj
    card.statusBar.addElement(elementName: global.gameConfig.tilesID, elementValue: global.gameConfig.getCurrentOptionString(option: global.gameConfig.tilesID, new: false), labelWidth: adjustedLabelWidth, xPosition: adjSep, icon: TileIcon(size: statusBarHeight))
    
    card.statusBar.addLabelsToView()
  }
  
  
  private func updateLengthLabel(Notification: Notification) -> Void {
    card.statusBar.updateLabel(elementName: global.gameConfig.lengthID, updatedValue: global.gameConfig.getCurrentOption(option: global.gameConfig.lengthID, new: false))
  }
  
  private func updateTimeLabel(Notification: Notification) -> Void {
    card.statusBar.updateLabel(elementName: global.gameConfig.timeID, updatedValue: global.gameConfig.getCurrentOption(option: global.gameConfig.timeID, new: false))
  }
  
  private func updateLexiconLabel(Notification: Notification) -> Void {
    card.statusBar.updateLabel(elementName: global.gameConfig.lexiconID, updatedValue: global.gameConfig.getCurrentOption(option: global.gameConfig.lexiconID, new: false))
  }
  
  private func updateTiles(Notification: Notification) -> Void {
    card.statusBar.updateLabel(elementName: global.gameConfig.tilesID, updatedValue: global.gameConfig.getCurrentOption(option: global.gameConfig.tilesID, new: false))
  }

  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  
  override func displayCard() {
    
    let initialOffest = cardHeight * 0.18
    let innerOffset = cardHeight * 0.125
    
    let length = SettingsFieldView(cardWidth: cardWidth, cardHeight: cardHeight, yPosition: initialOffest, configItem: global.gameConfig.lengthID, choices: global.gameConfig.getOptions(identifer: global.gameConfig.lengthID), text: "Length")
    
    let time = SettingsFieldView(cardWidth: cardWidth, cardHeight: cardHeight, yPosition: initialOffest + innerOffset, configItem: global.gameConfig.timeID, choices: global.gameConfig.getOptions(identifer: global.gameConfig.timeID), text: "Time")
    
    let lexicon = SettingsFieldView(cardWidth: cardWidth, cardHeight: cardHeight, yPosition: initialOffest + (innerOffset * 2), configItem: global.gameConfig.lexiconID, choices: global.gameConfig.getOptions(identifer: global.gameConfig.lexiconID), text: "Lexicon")
    
    let tiles = SettingsFieldView(cardWidth: cardWidth, cardHeight: cardHeight, yPosition: initialOffest + (innerOffset * 3), configItem: global.gameConfig.tilesID, choices: global.gameConfig.getOptions(identifer: global.gameConfig.tilesID), text: "Tiles")
    
    card.displayViews.append(length as UIView)
    card.displayViews.append(time as UIView)
    card.displayViews.append(lexicon as UIView)
    card.displayViews.append(tiles as UIView)
    
    addSettingsObservers()
    sharedDisplayCard()
  }
  
  override func hideCard() {
    sharedHideCard()
  }
  
  
  private func addSettingsObservers() {
    updateLengthObserver = NotificationCenter.default.addObserver(forName: Notification.Name.didChangeLength,
                                                                  object: nil,
                                                                  queue: OperationQueue.main,
                                                                  using: updateLengthLabel)
    
    updateTimeObserver =  NotificationCenter.default.addObserver(forName: Notification.Name.didChangeTime,
                                                                 object: nil,
                                                                 queue: OperationQueue.main,
                                                                 using: updateTimeLabel)
    
    updateLexiconObserver = NotificationCenter.default.addObserver(forName: Notification.Name.didChangeLexicon,
                                                                   object: nil,
                                                                   queue: OperationQueue.main,
                                                                   using: updateLexiconLabel)
    
    updateTilesObserver = NotificationCenter.default.addObserver(forName: Notification.Name.didChangeTiles,
                                                                 object: nil,
                                                                 queue: OperationQueue.main,
                                                                 using: updateTiles)
  }
  
  private func removeSettingsObservers() {
    if updateLengthObserver != nil {
      NotificationCenter.default.removeObserver(updateLengthObserver!)
    }
    if updateTimeObserver != nil {
      NotificationCenter.default.removeObserver(updateTimeObserver!)
    }
    if updateLexiconObserver != nil {
      NotificationCenter.default.removeObserver(updateLexiconObserver!)
    }
    if updateTilesObserver != nil {
      NotificationCenter.default.removeObserver(updateTilesObserver!)
    }
  }
  
}
