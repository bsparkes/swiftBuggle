//
//  StatClass.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import Foundation

class StatClass: Codable {
  
  let name: String
  var date: String
  var board: Dictionary<TileIndex, String>
  var value: String
  
  init(name n: String, value v: String, date d: String, board b: Dictionary<TileIndex, String>) {
    
    name = n
    date = d
    board = b
    value = v
  }
}
