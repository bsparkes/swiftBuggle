//
//  StatCollection.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import Foundation


class StatCollection: Codable {
  
  private enum CodingKeys: String, CodingKey {
    case config, mostWords, mostPoints, highestPercent, longestWord, longestHeterogram
  }
  
  let config: String
  var mostWords: StatClass
  var mostPoints: StatClass
  var highestPercent: StatClass
  var longestWord: StatClass
  var longestHeterogram: StatClass
  
  init(config c: String) {
    
    config = c
    
    mostWords = StatClass(name: "mostWords", value: "0", date: "", board: Dictionary<TileIndex, String>())
    mostPoints = StatClass(name: "mostPoints", value: "0", date: "", board: Dictionary<TileIndex, String>())
    highestPercent = StatClass(name: "highestPercent", value: "0", date: "", board: Dictionary<TileIndex, String>())
    longestWord = StatClass(name: "longestWord", value: "––", date: "", board: Dictionary<TileIndex, String>())
    longestHeterogram = StatClass(name: "longestHeterogram", value: "––", date: "", board: Dictionary<TileIndex, String>())
  }
  
  
  func saveStatCollection() -> Void {
    
    print("saving stats")
    let statIdentifier = global.gameConfig.getConfigIdentifyingString()
    
    let encoder = PropertyListEncoder()
    
    let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(statIdentifier + ".plist")
    do {
      let data = try encoder.encode(self)
      try data.write(to: filename)
      print("gameConfig saved to docs")
    } catch {
      print("error writing")
    }
  }  
}


func loadStatCollection() -> StatCollection {
  
//  print("loading stats")
  let statIdentifier = global.gameConfig.getConfigIdentifyingString()
  
  let decoder = PropertyListDecoder()
  let filename = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent(statIdentifier + ".plist")
  
  do {
    let data = try Data(contentsOf: filename)
    let decodedStats = try decoder.decode(StatCollection.self, from: data)
    
//    print("loaded stats")
    
    return decodedStats
  } catch {
    print("couldn't load stats")
    return StatCollection(config: global.gameConfig.getConfigIdentifyingString())
  }
  
}
