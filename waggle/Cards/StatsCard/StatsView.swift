//
//  StatsView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit


class StatsView : UIView {
  
  private let width: CGFloat
  private let height: CGFloat
  private let xPos: CGFloat
  private let yPos: CGFloat
  
  private let titleHeight: CGFloat
  private let valueHeight: CGFloat
  private let dateHeight: CGFloat
  private let spacing: CGFloat
  
  private let statTextColour = statusTextColour
  
  private let valIndet: CGFloat
  
  private let title: String
  private let value: String
  private let date: String
  private let board: Dictionary<TileIndex, String>
  
  init(xPos x: CGFloat, yPos y: CGFloat, width w: CGFloat, height h: CGFloat, title t: String, stat s: StatClass) {
    
    width = w
    height = h
    xPos = x
    yPos = y
    
    titleHeight = (CGFloat(3)/7)*height
    valueHeight = (CGFloat(2)/7)*height
    dateHeight = (CGFloat(1)/7)*height
    spacing = ((CGFloat(1)/7)*height)/4
    valIndet = width*0.05
    
    title = t
    value = s.value
    date = s.date
    board = s.board
    
    super.init(frame: CGRect(x: xPos, y: yPos, width: w, height: h))
    
    backgroundColor = settingsCardColour
    layer.cornerRadius = getCornerRadius(width: height * 0.95)
    addMiniGameBoard()
    addTitle()
    addValue()
    addDate()
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func addMiniGameBoard() {
    
    let boardView = GameboardView(boardSize: height * 0.95, abstractTiles: board)
    boardView.layer.position.x += width - height * 0.975
    boardView.layer.position.y += height * 0.025
    addSubview(boardView)
    
  }
  
  func addTitle() {
    
    let titleView = UILabel(frame: CGRect(x: height*0.025, y: spacing, width: width - height, height: titleHeight))
    titleView.backgroundColor = UIColor.clear
    titleView.text = title
    titleView.textColor = statTextColour
    titleView.font = UIFont(name: italicFontName, size: 0.725*titleHeight)
    addSubview(titleView)
    
  }
  
  func addValue() {
    let valueView = UILabel(frame: CGRect(x: height*0.025 + valIndet, y: spacing*2 + titleHeight, width: width - height - valIndet, height: valueHeight))
    valueView.backgroundColor = UIColor.clear
    valueView.text = value
    valueView.textColor = statTextColour
    let size = getFontSizeForRect(size: valueView.layer.bounds.size, accuracy: 1, text: value)
    valueView.font = UIFont(name: textFontName, size: size)
    addSubview(valueView)
    
  }
  
  func addDate() {
    let dateView = UILabel(frame: CGRect(x: height*0.025 + valIndet, y: spacing*3 + titleHeight + valueHeight, width: width - height - valIndet, height: dateHeight))
    dateView.backgroundColor = UIColor.clear
    dateView.textColor = statTextColour
    dateView.text = date
    dateView.font = UIFont(name: italicFontName, size: 0.8*dateHeight)
    addSubview(dateView)
  }
  
  
  func getFontSizeForRect(size s: CGSize, accuracy a: CGFloat, text t: String) -> CGFloat {
    // Binary search to find a font size that matches given size within some accuracy
    // Help from:
    // https://stackoverflow.com/questions/2844397/how-to-adjust-font-size-of-label-to-fit-the-rectangle/14662750#14662750
    // https://stackoverflow.com/questions/30450434/figure-out-size-of-uilabel-based-on-string-in-swift
    
    var minFontSize = CGFloat(5)
    var maxFontSize = CGFloat(50)
    var fontFitSize = s
    
    while (maxFontSize - minFontSize) > a {
      let midFontSize = CGFloat(maxFontSize + minFontSize)/2
      let text = NSAttributedString(string: t, attributes: [NSAttributedString.Key.font : UIFont(name: textFontName, size: midFontSize)!])
      fontFitSize = text.size()
      if fontFitSize.height <= s.height && fontFitSize.width <= s.width {
        minFontSize = midFontSize
      } else {
        maxFontSize = midFontSize
      }
    }
    return CGFloat(maxFontSize + minFontSize)/2
  }
}
