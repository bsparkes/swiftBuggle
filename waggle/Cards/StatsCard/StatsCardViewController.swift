//
//  StatsCardViewController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 18/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit


class StatsCardViewController: CardViewController {
  
  private var configChangeObserver: Any?
  private var statsUpdateObserver: Any?

  override init(cardName name: String, cardWidth width: CGFloat, cardColour colour: CGColor) {
    
    
    super.init(cardName: name, cardWidth: width, cardColour: colour)
    
    let statsCollection = loadStatCollection()
    
    card.statusBar.addElement(elementName: "maxFound", elementValue: statsCollection.mostWords.value, labelWidth: card.statusBar.frame.width/2, xPosition: 0)
    card.statusBar.addElement(elementName: "maxScore", elementValue: statsCollection.mostPoints.value, labelWidth: card.statusBar.frame.width/2, xPosition: card.statusBar.frame.width/2)
    card.statusBar.addLabelsToView()
    
    configChangeObserver = NotificationCenter.default.addObserver(forName: Notification.Name.didChangeConfig,
                                                                 object: nil,
                                                                 queue: OperationQueue.main,
                                                                 using: updateStatsStatus)
    
    statsUpdateObserver = NotificationCenter.default.addObserver(forName: Notification.Name.didUpdateStats,
                                                                  object: nil,
                                                                  queue: OperationQueue.main,
                                                                  using: updateStatsStatus)
  }
  
  override func displayCard() {
    addStats()
    sharedDisplayCard()
  }
  
  override func hideCard() {
    sharedHideCard()
  }
  
  
  func updateStatsStatus(Notification: Notification) -> Void {
//    print("UPDATING STATS ON NOTIFICATION")
    
    let statsCollection = loadStatCollection()
    card.statusBar.updateLabel(elementName: "maxFound", updatedValue: Int(statsCollection.mostWords.value) ?? 0)
    card.statusBar.updateLabel(elementName: "maxScore", updatedValue: Int(statsCollection.mostPoints.value) ?? 0)
  }
  
  required init(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func addStats() {
    
    let statCollection = loadStatCollection()
    
    let statIndent = cardWidth*0.0125
    let statHeight = (cardHeight*0.9 - statIndent*5)/5
    let statWidth = cardWidth*0.975
    
    let mostWordsFound = StatsView(xPos: statIndent, yPos: cardHeight*0.1, width: statWidth, height: statHeight, title: "Most Words Found", stat: statCollection.mostWords)
    
    card.displayViews.append(mostWordsFound)
    
    let highestPercent = StatsView(xPos: statIndent, yPos: cardHeight*0.1 + 2*(statHeight + statIndent), width: statWidth, height: statHeight, title: "Highest Found Ratio", stat: statCollection.highestPercent)
    
    card.displayViews.append(highestPercent)
    
    let mostPoints = StatsView(xPos: statIndent, yPos: cardHeight*0.1 + (statHeight + statIndent), width: statWidth, height: statHeight, title: "Most Points", stat: statCollection.mostPoints)
    
    card.displayViews.append(mostPoints)
    
    let longestWord = StatsView(xPos: statIndent, yPos: cardHeight*0.1 + 3*(statHeight + statIndent), width: statWidth, height: statHeight, title: "Longest Word", stat: statCollection.longestWord)
    
    card.displayViews.append(longestWord)
    
    let longestHeterogram = StatsView(xPos: statIndent, yPos: cardHeight*0.1 + 4*(statHeight + statIndent), width: statWidth, height: statHeight, title: "Longest Heterogram", stat: statCollection.longestHeterogram)
    
    card.displayViews.append(longestHeterogram)
  }
}
