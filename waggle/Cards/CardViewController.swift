//
//  cardViewController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 16/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

/* A controller for the card view.
 * Sets up a card, and adds a status bar to it.
 */
class CardViewController: UIViewController {
  
  var cardName: String
  var cardWidth: CGFloat
  var cardHeight: CGFloat
  var cardColour: CGColor
  var card: CardView
  
  
  init(cardName name: String, cardWidth width: CGFloat, cardColour colour: CGColor) {
    cardName = name
    cardWidth = width
    cardHeight = cardWidth * 1.4
    cardColour = colour
    
    card = CardView(cardName: cardName, cardWidth: cardWidth, cardColour: cardColour)
    
    super.init(nibName: nil, bundle: nil)
  }
  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func setCardViewYPosition(yPosition: CGFloat) {
    card.frame.origin.y = yPosition
  }
  
  func setCardViewXPosition(xPosition: CGFloat) {
    card.frame.origin.x = xPosition
  }
  
  func getCardView() -> CardView {
    return card
  }
  
  func addToView(enclosingView : UIView) {
    enclosingView.addSubview(card)
  }
  
  func addGesture(gesture: UIGestureRecognizer) {
    card.addGestureRecognizer(gesture)
  }
  
  override var shouldAutorotate: Bool {
    return true
  }
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    get {
      return .portrait
    }
  }
  
  
  func sharedDisplayCard() -> Void {
    for view in card.displayViews {
      card.addSubview(view)
    }
  }
  
  
  func displayCard() -> Void {
    fatalError("displayCard must be overwritten by specific card controller")
  }
  
  
  func sharedHideCard() -> Void {
    for view in card.displayViews {
      view.removeFromSuperview()
    }
    card.displayViews.removeAll()
  }
  
  func hideCard() -> Void {
    fatalError("hideCard must be overridden by specific card controller")
  }
  
}
