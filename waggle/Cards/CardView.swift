//
//  cardView.swift
//  buggle
//
//  Created by Benjamin Sparkes on 29/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

/* A basic UIView wrapper to display a card.
 */
class CardView : UIView {
  
  let cardName: String
  let cardWidth: CGFloat
  let cardHeight: CGFloat
  let cardColour: CGColor
  
  let statusBar: StatusBarUIView
  
  // displayViews collects the views which should only be shown when the card is displayed.
  var displayViews = [UIView]()
  
  
  init(cardName n: String, cardWidth width: CGFloat, cardColour color: CGColor) {
    cardName = n
    cardWidth = width
    cardHeight = width*1.4
    cardColour = color
    statusBar = StatusBarUIView(cardWidth: cardWidth, cardHeight: cardHeight)
    
    super.init(frame: CGRect(x: 0, y: 0, width: cardWidth, height: cardHeight))
    
    layer.cornerRadius = getCornerRadius(width: cardWidth)
    layer.backgroundColor = cardColour
    
    addSubview(statusBar)
  }

  
  required init(coder: NSCoder) {
    fatalError("NSCoding not supported")
  }
}

