//
//  modelData.swift
//  buggle
//
//  Created by Benjamin Sparkes on 30/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit
import CoreData

class DeallocPrinter {
  deinit {
    print("deallocated model")
  }
}

class MasterClass {
  var managedContext: NSManagedObjectContext?
  let wordTrie: CoreTrie
  
  var gameConfig: GameConfiguration
  var gameModel: GameModel
  
  init() {
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    managedContext = appDelegate.persistentContainer.viewContext
    
    gameConfig = loadGameConfig()
    let loadedModel = loadModel()
    if loadedModel != nil {
      gameModel = loadedModel!
    }else {
      gameModel = gameConfig.makeGameModel()
    }
    
    print("global items made")
    print("making wordtrie")
    self.wordTrie = CoreTrie()
  }
}

let global = MasterClass()

//MARK: - GOOD VARIABLE

//var gameConfig = loadGameConfig()
//var gameModel = loadModel()
var finishedTrie = false

//MARK: - GOOD CONSTANTS

let cardStack = CardStackViewController()




// MARK: - BADISH CONSTANTS

// MARK: - MOVE ELSEWHERE

struct alphabet {
  let model: [String] = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "!", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
}
