//
//  ViewController.swift
//  buggle
//
//  Created by Benjamin Sparkes on 19/06/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit
import CoreData

class CardStackViewController: UIViewController {
  
  private let width: CGFloat
  private let cardIndent: CGFloat
  
  private var cardViews = [CardView]()
  private var cardOrigin: CGFloat = 0.0
  
  let gameCard: GameCardViewController
  let settingsCard: SettingsCardViewController
  let statsCard: StatsCardViewController
  
  private var gameCardGesture: UIGestureRecognizer?
  private var settingsCardGesture: UIGestureRecognizer?
  private var statsCardGesture: UIGestureRecognizer?
  
  
  
  init() {
    print("CardStackViewController")
    
    width = min(((UIScreen.main.bounds.size.height)/1.4/1.16)*0.9, UIScreen.main.bounds.size.width)
    cardIndent = (UIScreen.main.bounds.size.width - width)/2
    
    gameCard = GameCardViewController(cardName: "gameCard", cardWidth: width, cardColour: gameCardColour.cgColor)
    settingsCard = SettingsCardViewController(cardName: "settingsCard", cardWidth: width, cardColour: settingsCardColour.cgColor)
    statsCard = StatsCardViewController(cardName: "statsCard", cardWidth: width, cardColour: statsCardColour.cgColor)
    
    super.init(nibName: nil, bundle: nil)
  }
  
  convenience required init?(coder aDecoder: NSCoder)
  {
    self.init()
  }
  
  
  override func viewDidLoad() {
    
    let cardSpace = (view.bounds.size.height - ((width*1.4)*1.16))/2
    
    cardOrigin = cardSpace + (width*1.4)*0.16
    
    // set the initial card positions
    gameCard.setCardViewYPosition(yPosition: cardOrigin)
    settingsCard.setCardViewYPosition(yPosition: cardOrigin - (width*1.4)*0.08)
    statsCard.setCardViewYPosition(yPosition: cardOrigin - (width*1.4)*0.16)
    gameCard.setCardViewXPosition(xPosition: cardIndent)
    settingsCard.setCardViewXPosition(xPosition: cardIndent)
    statsCard.setCardViewXPosition(xPosition: cardIndent)
    
    // add cards to view in correct order
    statsCard.addToView(enclosingView: view)
    settingsCard.addToView(enclosingView: view)
    gameCard.addToView(enclosingView: view)
    
    //update list in correct order
    cardViews.append(gameCard.getCardView())
    cardViews.append(settingsCard.getCardView())
    cardViews.append(statsCard.getCardView())
    
    // Add gesture
    gameCardGesture = UITapGestureRecognizer(target: self, action: #selector(CardStackViewController.bringCardToFront(_:)))
    settingsCardGesture = UITapGestureRecognizer(target: self, action: #selector(CardStackViewController.bringCardToFront(_:)))
    statsCardGesture = UITapGestureRecognizer(target: self, action: #selector(CardStackViewController.bringCardToFront(_:)))
    gameCard.addGesture(gesture: gameCardGesture!)
    settingsCard.addGesture(gesture: settingsCardGesture!)
    statsCard.addGesture(gesture: statsCardGesture!)
    
    gameCardGesture!.isEnabled = false
    settingsCardGesture!.isEnabled = true
    statsCardGesture!.isEnabled = true
    
    // Testing area
  }
  
  
  @objc func bringCardToFront(_ r: UIGestureRecognizer!) {
    
    // Get the selected card
    let cardIndex = cardViews.firstIndex(of: r.view! as! CardView)
    
    //Remove from current index, and add it at index 0
    cardViews.remove(at: cardIndex!)
    cardViews.insert(r.view! as! CardView, at: 0)
    
    // Ensure gestures are only available for the respective card.
    // This could be simplified by adding the gesture to the card view class,
    // but it works as it is.
    switch cardViews[0].cardName {
    case "gameCard":
      gameCardGesture!.isEnabled = false
      settingsCardGesture!.isEnabled = true
      statsCardGesture!.isEnabled = true
    case "settingsCard":
      gameCardGesture!.isEnabled = true
      settingsCardGesture!.isEnabled = false
      statsCardGesture!.isEnabled = true
    case "statsCard":
      gameCardGesture!.isEnabled = true
      settingsCardGesture!.isEnabled = true
      statsCardGesture!.isEnabled = false
    default:
      gameCardGesture!.isEnabled = false
      settingsCardGesture!.isEnabled = false
      statsCardGesture!.isEnabled = false
    }
    
    if cardViews[0].cardName == "gameCard" {
    } else {
      if global.gameModel.timeRemaining != 0 { // test to see whether the game is over
        gameCard.pauseGame(animated: false)
      }
      gameCard.hideCard()
    }
    
    if cardViews[0].cardName == "settingsCard" {
      settingsCard.displayCard()
    } else {
      settingsCard.hideCard()
    }
    
    if cardViews[0].cardName == "statsCard" {
      statsCard.displayCard()
    } else {
      statsCard.hideCard()
    }
    reorderCardDisplay()
  }
  
  func reorderCardDisplay() {
    UIImpactFeedbackGenerator(style: .medium).impactOccurred()
    view.bringSubviewToFront(cardViews[0])
    var yIndex = CGFloat(0)
    for i in 0 ..< cardViews.count {
      UIView.animate(withDuration: 0.25, animations: {
        self.cardViews[i].frame.origin.y = self.cardOrigin + yIndex
      })
      yIndex -= (width*1.4)*0.08
    }
  }
  
}




