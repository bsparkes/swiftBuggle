//
//  UIRelated.swift
//  buggle
//
//  Created by Benjamin Sparkes on 24/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import UIKit

//class LayerRemover: NSObject, CAAnimationDelegate {
//  private weak var layer: CALayer?
//  
//  init(for layer: CALayer) {
//    self.layer = layer
//    super.init()
////    print("remover init")
////    print(self.layer?.animationKeys())
//  }
//  
//  func animationDidStop(_ anim: CAAnimation, finished flag: Bool) {
//    print("OOOOO")
////    print(layer?.animationKeys())
////    print(layer?.sublayers)
////    layer?.removeAllAnimations()
////    print(layer?.animationKeys())
//  }
//}


let statusTextColour = UIColor.lightGray
let statusTextBackground = UIColor.gray

let gameCardColour = UIColor.black
let settingsCardColour = UIColor.darkGray
let statsCardColour = UIColor.gray

let regularText = UIColor.black
let reguardTextShaded = UIColor.darkGray

let gameCardIndent = UIColor.darkGray
let gameCardOutdent = UIColor.lightGray

let tileOutlineColour = UIColor.black
let tileStrokeColour = UIColor.darkGray
let tileFillColour = UIColor.gray
let tileSelectedColour = UIColor.white
let tileBackgroundColour = UIColor.lightGray

let boardBackgroundColour = UIColor.darkGray
let gameCardElementBackgroundColour = UIColor.gray

let interactiveStrokeColour = UIColor.black

let settingsMenuBackgroundColour = UIColor.lightGray
let settingsTextColour = UIColor.darkGray
let settingsTextSelectedColour = UIColor.lightGray

let tileAnimDuration = 0.5

let defaultAnimationDuration = 1.0


let gCardFontSize = CGFloat(12)
let gCardFont = UIFont(name: uiFontName, size: gCardFontSize)
let tileFontName = "K2D-ExtraBold"
let uiFontName = "K2D-ExtraBold"
let textFontName = "K2D-Regular"
let italicFontName = "K2D-Italic"



func getCornerRadius(width w: CGFloat) -> CGFloat {
  return w*(1/30)
}
