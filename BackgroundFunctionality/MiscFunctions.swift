//
//  MiscFunctions.swift
//  buggle
//
//  Created by Benjamin Sparkes on 21/06/2019.
//  Copyright © 2019 Benjamin Sparkes. All rights reserved.
//

import UIKit
  
func getIntersection(circleOne cO: CGPoint, radiusOne rO: CGFloat, circleTwo cT: CGPoint, radiusTwo rT: CGFloat) -> [CGPoint] {
  // Help from https://math.stackexchange.com/questions/256100/how-can-i-find-the-points-at-which-two-circles-intersect
  // and https://gist.github.com/jupdike/bfe5eb23d1c395d8a0a1a4ddd94882ac
  
  let d = sqrt(pow(cO.x - cT.x, 2) + pow(cO.y - cT.y, 2))
  
  if d > rO + rT || d < abs(rT - rO) || (d == 0 && rO == rT) {
    return []
  } else {
    
    let d2 = pow(d,2)
    let d4 = pow(d,4)
    
    let a = (pow(rO,2) - pow(rT,2)) / (2*d2)
    let r1r2 = (pow(rO,2) - pow(rT,2))
    let c = sqrt((2*(pow(rO,2) + pow(rT,2))) / d2) - ((pow(r1r2,2) / d4) - 1)
    
    let fx = (cO.x + cT.x) / 2 + a * (cT.x - cO.x)
    let gx = c * (cT.y - cO.y) / 2
    
    let ix1 = fx + gx
    let ix2 = fx - gx
    
    let fy = (cO.y + cT.y) / 2 + a * (cT.y - cO.y)
    let gy = c * (cO.x - cT.x) / 2
    
    let iy1 = fy + gy
    let iy2 = fy - gy
    
    return [CGPoint(x: ix1, y: iy1), CGPoint(x: ix2, y: iy2)]
  }
}

func heterogramTest(word w: String) -> Bool {
  
  var wordSet = Set<String.Element>()
  
  for char in w {
    if wordSet.contains(char) {
      return false
    } else {
      wordSet.insert(char)
    }
  }
  return true
}


func logw(m: String) {
  let main = Thread.current.isMainThread
  let name = main ? "[main]" : "[back]"
  print("\(name) \(m)")
}


//func bagToTileSet(bag b: String) -> [String] {
//  
//  let bag = diceSet[b]!
//  var selection = [String]()
//  
//  for i in 0 ..< bag.count {
//    let index = Int(arc4random_uniform(UInt32(bag[0].count)))
//    selection.append(bag[i][index])
//  }
//  selection = selection.shuffled()
//  
//  return selection
//}


let basicDistribution = [  // https://en.wikipedia.org/wiki/Letter_frequency
  8.167,
  1.492,
  2.202,
  4.253,
  12.702,
  2.228,
  2.015,
  6.094,
  6.966,
  0.153,
  1.292,
  4.025,
  2.406,
  6.749,
  7.507,
  1.929,
  0.095,
  5.987,
  6.327,
  9.356,
  2.758,
  0.978,
  2.560,
  0.150,
  1.994,
  0.077
]

// PROBS
func randomNumber(distribution d: [Double]) -> Int {
  
//  let sum = d.reduce(0, +)
   
   let rnd = Double.random(in: 0.000 ..< 100)
   
   var accum = 0.0
   for (i,p) in d.enumerated() {
     accum += p
     if rnd < accum {
       return i
     }
   }
   return (d.count - 1)
}
