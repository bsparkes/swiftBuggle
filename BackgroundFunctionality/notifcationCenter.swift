//
//  notificationCenter.swift
//  buggle
//
//  Created by Benjamin Sparkes on 18/12/2018.
//  Copyright © 2018 Benjamin Sparkes. All rights reserved.
//

import Foundation

extension Notification.Name {
  static let didChangeLength = Notification.Name("didChangeLength")
  static let didChangeTime = Notification.Name("didChangeTime")
  static let didChangeLexicon = Notification.Name("didChangeLexicon")
  static let didChangeTiles = Notification.Name("didChangeTiles")
  static let didFindWord = Notification.Name("didFindWord")
  static let didChangeConfig = Notification.Name("didChangeConfig")
  static let didUpdateStats = Notification.Name("didUpdateStats")
}
